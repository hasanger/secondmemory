package io.sanger.henry.secondmemory.data;

/**
 * The {@code Settings} class contains all the settings for SecondMemory.
 * @author gitlabcyclist
 * @version 0.4 beta 
*/
public class Settings {
	
	public static String prompt = "->"; // Prompt. This is printed below the task list to indicate the place where commands are entered.
	
	public static String questionListTopChar = "-"; // The character used for drawing the top and bottom of the question list

	public static String questionListSideChar = "|"; // The character used for drawing the sides of the question list
	
	public static boolean useBoxDrawing = false; // If set to true, then SecondMemory will ignore the question list side and top chars and print the question list using box drawing chars.
	
	public static boolean showCommands = true; // If set to true, then a list of available commands will print below the question list
	
	public static boolean showWorkload = false; // If set to true, then the workload will print below the question list
	
	public static boolean autoMoveOn = false; // If set to true, then question creation/narrative question review/question editing will move on to the next step when the editor is closed
	
	public static boolean header = true; // Determines whether the header should be shown or not.
	
	public static boolean showExtraNumbers = true; // Determines whether or not to show the ID of each question in the question list
	
	public static int questionLengthLimit = 48; // Maximum length a question can be in the question list before being truncated
	
	// Question limit.
	// A warning will be shown if you attempt to schedule a question for a date
	// that is either over the limit or will be when you schedule a question for the date.
	public static int questionLimit = 25; 

}
