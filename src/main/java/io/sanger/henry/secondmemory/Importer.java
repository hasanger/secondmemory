package io.sanger.henry.secondmemory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import io.sanger.henry.secondmemory.question.Question;
import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarStyle;

public class Importer {
	
	public static ArrayList<Question> importQACollection(File collection) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(collection));
		int lines = 0;
		String line2;
		while ((line2 = reader.readLine()) != null) {
			if(line2.contains("Q:") || line2.contains("A:")) lines++;
		}
		reader.close();
		lines = lines / 2;
		try (ProgressBar pb = new ProgressBar("Loading questions", lines, ProgressBarStyle.ASCII)) {
			Question current = new Question();
			ArrayList<Question> questions = new ArrayList<>();
			try(BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(collection), "UTF-8"))) { // UTF-8 so special characters don't show up as question marks
			    String line;
			    while ((line = in.readLine()) != null) {
					if(line.contains("Q:")) {
						String question = line.substring(2);
						current.question = question.trim();
					} else if(line.contains("A:")) {
						String answer = line.substring(2);
						current.answer = answer.trim();
					} else {
						pb.step();
						questions.add(current);
						current = new Question();
					}
			    }
			}
			return questions;
		}
	}
}
