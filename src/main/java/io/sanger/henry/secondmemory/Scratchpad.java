package io.sanger.henry.secondmemory;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.*;
import java.io.IOException;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.Document;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;

public class Scratchpad extends JFrame {
	
	private static final long serialVersionUID = -2762368636167955607L;
	
	private JTextArea editorArea; // The text area that the 
	private String currentFile = ""; // The currently open file. Used to determine which file to write to.
    
	private String lookAndFeel = "";

	public Scratchpad() {
		
		// Set look and feel to OS look and feel, or Nimbus on Linux.
		// Any non-Mac look and feel will 
		try {
			Utils.osDependent(() -> lookAndFeel = UIManager.getSystemLookAndFeelClassName(), // Windows
						() -> lookAndFeel = "javax.swing.plaf.nimbus.NimbusLookAndFeel", // Linux
						() -> lookAndFeel = UIManager.getSystemLookAndFeelClassName()); // Mac
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			UIManager.setLookAndFeel(lookAndFeel);
		} catch (Exception e) {
			Utils.error(e);
		}
		
		// Set basic properties of frame:
		this.setTitle("Scratchpad"); // Title
		this.setSize(300, 400); // Default size
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE); // Close operation
		this.setLayout(new BorderLayout()); // Layout
		
		this.addWindowListener(new WindowListener() {
			
			@Override public void windowOpened(WindowEvent e) {}		
			@Override public void windowIconified(WindowEvent e) {}		
			@Override public void windowDeiconified(WindowEvent e) {}	
			@Override public void windowDeactivated(WindowEvent e) {}
			@Override
			public void windowClosing(WindowEvent e) {
				// Save on close, even if not saved by user. This eliminates
				// the need to use the save shortcut or button in most cases.
				try {
					Utils.populateFileInCWD(currentFile, editorArea.getText());
				} catch (IOException e1) {
					Utils.basicRedPrint("There was a problem writing to a file.");
					Utils.error(e1);
				}
			}
			@Override public void windowActivated(WindowEvent e) {}
			@Override public void windowClosed(WindowEvent e) {}
			
		});
		
		// Initialize text area
		editorArea = new JTextArea();
		editorArea.setBorder(new BevelBorder(BevelBorder.LOWERED));
		editorArea.setFont(new Font("Times New Roman", Font.PLAIN, 18));
		editorArea.setWrapStyleWord(true);
		
		this.add(editorArea, BorderLayout.CENTER); // Add it to the frame
		
		// Thanks to https://stackoverflow.com/a/47988281 for the following code
	    UndoManager undoManager = new UndoManager();

	    Document document = editorArea.getDocument();
	    document.addUndoableEditListener(new UndoableEditListener() {
	        @Override
	        public void undoableEditHappened(UndoableEditEvent e) {
	            undoManager.addEdit(e.getEdit());
	        }
	    });
	    // End of code from https://stackoverflow.com/a/47988281
		
		JMenuBar editorBar = new JMenuBar(); // Create menu bar
		
		// Initialize menus:
		
		// File menu
		JMenu fileMenu = new JMenu("File");
		JMenuItem saveItem = new JMenuItem("Save");
		saveItem.addActionListener(e -> {
			try {
				Utils.populateFileInCWD(currentFile, editorArea.getText());
			} catch (IOException e1) {
				Utils.basicRedPrint("There was a problem writing to a file.");
				Utils.error(e1);
			}
		});
		saveItem.setAccelerator(KeyStroke.getKeyStroke('S', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask())); // Add keyboard shortcut
		fileMenu.add(saveItem);
		
		// Edit menu
		JMenu editMenu = new JMenu("Edit");
		JMenuItem undo = new JMenuItem("Undo");
		undo.setAccelerator(KeyStroke.getKeyStroke('Z', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		JMenuItem redo = new JMenuItem("Redo");
		redo.setAccelerator(KeyStroke.getKeyStroke('Y', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		
		// More code from https://stackoverflow.com/a/47988281: Adds action listeners to the undo and redo buttons & shortcuts
	    undo.addActionListener((ActionEvent e) -> {
	        try {
	        	undoManager.undo();
	        } catch (CannotUndoException cue) {}
	    });
	    redo.addActionListener((ActionEvent e) -> {
	        try {
	        	undoManager.redo();
	        } catch (CannotRedoException cre) {}
	    });
	    // End code from https://stackoverflow.com/a/47988281
	    
	    // Add undo and redo buttons to the edit menu
	    editMenu.add(undo);
	    editMenu.add(redo);
		
		
	    // Add menus
		editorBar.add(fileMenu);
		editorBar.add(editMenu);
		
		this.add(editorBar, BorderLayout.NORTH); // Add menu bar to the frame
	}
	
	public void setOpenFile(String filename) {
		this.currentFile = filename;
	}
	
	public void setEditorText(String text) {
		editorArea.setText(text);
	}
	
	public void showWindow() {
		this.setVisible(true);
		this.setAlwaysOnTop(true);
		this.setAlwaysOnTop(false);
	}

}
