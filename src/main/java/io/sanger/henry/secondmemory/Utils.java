package io.sanger.henry.secondmemory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.List;

import io.sanger.henry.secondmemory.data.Settings;
import io.sanger.henry.secondmemory.question.Question;
import io.sanger.henry.secondmemory.question.QuestionEntry;
import io.sanger.henry.secondmemory.question.QuestionType;
import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.Ansi.Color;
import org.fusesource.jansi.AnsiConsole;
import org.ocpsoft.prettytime.PrettyTime;

import com.joestelmach.natty.DateGroup;
import com.joestelmach.natty.Parser;

import info.debatty.java.stringsimilarity.Levenshtein;
import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarStyle;

/**
 * The {@code Utils} class contains most of the methods
 * and variables used in SecondMemory.
 * The main exceptions are the settings and statistics, which are stored in separate classes.
 * @author gitlabcyclist
 * @version 0.4 beta
*/
public abstract class Utils {
	
	///////////////
	// Variables //
	///////////////
	
	
	// Constants //
	
	public static final String PROGRAM_VERSION = "0.4 beta"; // Program version
	
	public static String CWD = Paths.get(".").toAbsolutePath().normalize().toString(); // CWD = current working directory
	
	public static String CWDS = CWD + File.separator; // CWDS = current working directory with system-specific path separator
	
	// The OS name. Used for finding the current OS.
	private static String osName = System.getProperty("os.name");
		
		
	// Variables that osDependent uses to determine the current OS.
	private static boolean isWindows = osName.contains("Windows"),
						   isMac = osName.contains("Mac"),
						   isLinux = osName.contains("Linux");
	
	// Objects //
	
	public static Scanner mainScanner = new Scanner(System.in); // Main scanner, used across all classes for user input
	
	public static ArrayList<Topic> topics = new ArrayList<>(); // List of topics
	
	public static Levenshtein l = new Levenshtein(); // Used to calculate Levenshtein distance
	
	public static Parser parser = new Parser(); // Natty parser, used for parsing natural dates like "in 3 days" or "tomorrow"
	
	public static DateTimeFormatter defaultFormat = DateTimeFormatter.ofPattern("MM/dd/yyyy"); // Formatter, used to print and parse some dates, including for JSON
	
	// Used to sort the main question list by next review date
	public static Comparator<Question> sorter = new Comparator<Question>() {
	    @Override
	    public int compare(Question o1, Question o2) {
	        return o1.nextReview.compareTo(o2.nextReview);
	    }
	};
	
	private static Scratchpad scratchpad = new Scratchpad(); // The Scratchpad

	public static PrettyTime prettyTime = new PrettyTime(); // Used to print (not parse) human-readable dates.
	
	public static BufferedWriter stdout = new BufferedWriter(new OutputStreamWriter(System.out)); // Used to print. Supposedly faster than System.out.print.
	
	
	public static Breadcrumb creationBreadcrumb = new Breadcrumb("Type", "Question", "Answer", "Topic");
	
	/////////////
	// Methods //
	/////////////
	
	// Equivalents to System.out.println(),
	// System.out.println(str), and System.out.print(str),
	// because they're used so much in SecondMemory.
	// I don't actually use System.out.print, because
	// the other method that I use is supposedly faster.
	
	public static void p() {
		p("");
	} 
	
	public static void p(String str) {
		ps(str + "\n");
	}
	
	public static void ps(String str) { // stands for "print short"
		try {
			stdout.append(str);
			stdout.flush();
		} catch (IOException e) {
			error(e);
		}
	}
	
	
	// Date parsing methods //
	
	
	// Date conversion methods: Thanks to https://howtodoinjava.com/java/date-time/localdate-to-date //
	
	
	public static Date localDateToDate(LocalDate localDate) {
		Date date = Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
		return date;
	} 
	
	public static LocalDate dateToLocalDate(Date date) {
		LocalDate localDate = Instant.ofEpochMilli(date.getTime())
						             .atZone(ZoneId.systemDefault())
						             .toLocalDate();
		return localDate;
	} 
	
	// End https://howtodoinjava.com/java/date-time/localdate-to-date/ methods //
	
	
	// Date formatting methods //
	
	public static String prettyTimeFormat(LocalDate localDate) {
    	
    	Date date = localDateToDate(localDate); // Convert the given LocalDate to a Date for the pretty time methods
    	
    	return (isToday(localDate) ? "today" // If the given date is today, then return "today"
    			
    			// If the given date contains "hours from now", then it's tomorrow, so in that case, return "tomorrow"
    			// Otherwise, return the prettyTime formatted date
				: (prettyTime.format(date).contains("hours from now") ? "tomorrow" : prettyTime.format(date)))
    			
    				// If the date is yesterday, the formatter outputs "1 day ago".
    				// If this is the case, replace it with "yesterday"
    				.replace("1 day ago", "yesterday");
    }
	
	// Takes a LocalDate and returns a string like "1/1/1970".
	// Used when saving data to JSON format, and when viewing a question.
	public static String jsonFormat(LocalDate date) {
		return defaultFormat.format(date);
	}
	
	// Similar to prettyTimeFormat, but uses a completely different algorithm.
	// Used in the drawWorkload method
	public static String workloadFormat(LocalDate localDate) {
		String returned = "";
		switch(daysFromToday(localDate)) {
		case 0:
			returned = "today";
			break;
		case 1:
			returned = "tomorrow";
			break;
		case -1:
			returned = "yesterday";
			break;
		case 7:
			returned = "a week from now";
			break;
		case -7:
			returned = "a week ago";
			break;
		default:
			if(localDate.isBefore(LocalDate.now())) {
				returned = daysFromTodayAbsolute(localDate) + " days ago";
			} else {
				returned = daysFromTodayAbsolute(localDate) + " days from now";
			}
			break;
		}
		return returned;
	}
	
	// Date parsing methods //
	
	// The difference between parseJSONDate and
	// parseDate is that the former is used for human-readable
	// dates, while the latter is used for reading dates
	// from questions.json.
	
	public static LocalDate parseJSONDate(String date) {
		try {
			return LocalDate.parse(date, defaultFormat);
		} catch (DateTimeParseException e) {
			error(e);
		}
		return LocalDate.now();
	}
	
	// Parses date using Natty, a natural language date parser
	public static LocalDate parseDate(String text) {
		List<DateGroup> groups = parser.parse(text);
		if(groups.size() < 1) return null;
		Date date = groups.get(0).getDates().get(0);
		return dateToLocalDate(date);
	}
	
	// More date methods //
	    
    // Checks if a LocalDate is today
    public static boolean isToday(LocalDate date) {
    	// Check if today at midnight is equal to the given day at midnight
    	return date.atStartOfDay().isEqual(LocalDate.now().atStartOfDay());
    }

	// Topic-related methods //
	
	// Finds the topic in the topic list (if any)
	// with the given name. If it's not found, a
	// red topic with (not found) appended is returned.
	// If "(not found)" is at the end of the topic name,
	// however, it's not added.
	public static Topic findTopic(String name) {
		for(Topic topic : topics) {
			if(topic.name.equals(name)) return topic;
		}
		return new Topic(name + (name.contains("(not found)") ? "" : " (not found)"), TopicColor.RED);
	}
	
	// Used during the question creation process,
	// and for editing questions.
	public static Topic makeTopicChoice(boolean forEditing) {
		int i = 1;
		for(Topic topic : topics) {
			TopicColor.printColor(topic.color);
			p("(" + i + ") " + topic.name);
			TopicColor.reset();
			i++;
		}
		while(true) {
			try {
				ps("Choose a topic by number, " + (forEditing ? "or:\n\"q\" - Quit editing\nEnter - Don't change topic" : "or q to quit") + "\n~> ");
				String chosenRaw = mainScanner.nextLine();
				int chosen = 0;
				if(!chosenRaw.equals("q") && !chosenRaw.equals("")) {
					chosen = Integer.parseInt(chosenRaw);
				} else if(chosenRaw.equals("")) {
					if(forEditing) return new Topic(true); // If the topic choice is being made during question editing, Enter should move on without changing the topic.
					else throw new NumberFormatException(); // If the topic choice is being made during question creation, Enter can be accidentally pressed and quit question creation without this part. 
				} else {
					return null;
				}
				return topics.get(chosen - 1); // - 1 because list starts at 1
			} catch(Exception e) {
				basicRedPrint("\nThat wasn't right. Press Enter and try again.\n");
				mainScanner.nextLine(); // Stop infinite looping
				try {
					Thread.sleep(50);
				} catch (InterruptedException e1) {
					error(e1);
				}
				return makeTopicChoice(forEditing);
			}
		}
	}
	
	// Draws a full question. Used for both displaying
	// questions on the view screen & confirming their
	// deletion.
	public static void drawQuestion(Question question) {
		TopicColor.printColor(question.topic.color);
		ps("(" + question.topic.name + ") ");
		TopicColor.reset();
		p(question.question);
	}
	
	// Reschedules all questions, moving them back or forward by the given number of days
	public static void moveQuestions(int rescheduleDays) {
		
		if(rescheduleDays == 0) return; // No need to do anything if there are no questions to reschedule!
		
		boolean decrement = false; // Determines whether to increment or decrement the date.
		
		if(rescheduleDays < 0) { // If the given number of days is negative, then...
			rescheduleDays = Math.abs(rescheduleDays); // Make it positive
			decrement = true;
		}
		
		try (ProgressBar pb = new ProgressBar("Rescheduling questions", SecondMemory.list.questions.size(), ProgressBarStyle.ASCII)) { // Progress bar to show the user the progress of the question rescheduling 
			
			for(Question question : SecondMemory.list.questions) { // Loop over all questions
				
				LocalDate rescheduleDate = question.nextReview; // New date to move this question to. Starts as the question's next review date
				
				// If decrement is true, subtract the given number of days; if it's false, add the given number of days
				if(decrement) {
					rescheduleDate = rescheduleDate.minusDays(rescheduleDays);
				} else {
					rescheduleDate = rescheduleDate.plusDays(rescheduleDays);
				}
				
				question.reschedule(rescheduleDate); // Change the question's next review date to the calculated reschedule date
				
				pb.step(); // Increment the progress bar
				
			}
			
		}
	}
 
	
	// Draws a list of questions. This method was moved from QuestionList when pagination was being
	// implemented, so that different screens of questions could be drawn.
    // TODO Clean up code, improve efficiency
	public static void draw(boolean header, String headerText, ArrayList<Question> questions, int startIndex, int endIndex) {
		
		String topChar, sideChar;
		if(Settings.useBoxDrawing) {
			topChar = "─"; sideChar = "│";
		} else {
			topChar = Settings.questionListTopChar; sideChar = Settings.questionListSideChar;
		}
		
		// If there are no questions in the list, print "No questions here" and leave the method
		if(questions.size() < 1) {
			if(header) header(32);
			p("No questions here.\n");
			return;
		}
		
		endIndex++;
		
		// Prevent IndexOutOfBoundsException.
		// If startIndex is too small (less than 0), make it 0.
		if(startIndex < 0) startIndex = 0;
		
		// Prevent IndexOutOfBoundsException.
		// If endIndex is too large, then make it the index of the last question.
		if(endIndex > questions.size()) endIndex = questions.size();
		
		int maxTopicLength = 0, // Length of the longest topic string
		    maxNameLength = 0, // Length of the longest question name string
		    maxTimeLength = 0, // Length of the longest time string
		    maxIDLength = 1 /*String.valueOf(endIndex).length()*/; // Length of the longest ID string
		
		ArrayList<QuestionEntry> entries = new ArrayList<>();
		 
		// Iterate over questions
		for(int i = startIndex; i < endIndex; i++) {
			
			// Create a Question object for the given index
			Question question = new Question();
			try {
				question = questions.get(i);
			} catch(ArrayIndexOutOfBoundsException aioobe) {
				error(aioobe);
				break;
			}
			
			QuestionEntry entry = new QuestionEntry();
			
			if(question.topic == null) question.topic = new Topic("No topic", TopicColor.RED); // If the question doesn't have a topic, give it a generic one
			
			entry.timeStr = (question.reviewedBefore ? prettyTimeFormat(question.nextReview) : "not scheduled"); // 
			
			// Name string and topic string
			entry.nameStr =
					(Settings.showExtraNumbers ? ("#" + i + ": ") : "")
					+ question.question; // If showExtraNumbers is true, then print the ID of the question; otherwise, print nothing extra
			entry.nameStr = entry.nameStr.replace("\r", " ")
										 .replace("\n", " ");
			entry.topicStr = question.topic.name;
			
			int currentTopicLength = entry.topicStr.length(), // The length of the topic string
			    currentNameLength = entry.nameStr.length(), // The length of the name string
			    currentTimeLength = entry.timeStr.length(); // The length of the time string
			
			if(currentNameLength > Settings.questionLengthLimit + 1) currentNameLength = Settings.questionLengthLimit + 1;
			if(currentTopicLength > 20) currentTopicLength = 21;
			
			// If the length of the current topic string is longer than the longest one recorded,
			// then set the longest one recorded (maxTopicLength) to the current topic string's length.
			// Same for the rest of the values
			if(currentTopicLength > maxTopicLength) maxTopicLength = currentTopicLength;
			
			if(currentNameLength > maxNameLength) maxNameLength = currentNameLength;
			
			if(currentTimeLength > maxTimeLength) maxTimeLength = currentTimeLength;
						
			// If the name string is greater than 48 chars, add an ellipsis
			if(entry.nameStr.length() > Settings.questionLengthLimit) {
				entry.nameStr = entry.nameStr.substring(0, (Settings.questionLengthLimit - 4)) + "...";
			}
						
			// Same with the topic string
			if(entry.topicStr.length() > 20) {
				entry.topicStr = entry.topicStr.substring(0, 18) + "...";
			}

			// If this question is in the past, then make inPast true;
			// otherwise, make it false. Same effect as an if statement,
			// but in a smaller package.
			entry.inPast = question.nextReview.isBefore(LocalDate.now());
			
			entries.add(entry); // Add the QuestionEntry to the array
			
		}
		
		// This variable declares how long the lines at the top and bottom of the question list should be.
		// To calculate it, the maximum length of each column is used, + 13 extra for spacers & spaces.
		int topBottomLen = maxIDLength + maxTopicLength + maxNameLength + maxTimeLength + 13;
		
		// The ternary is to make sure that there is never no space
		// between "SecondMemory" and the version number.
		int headerSize = (topBottomLen - 16 < 1 ? 1 : topBottomLen - 16);
		if(header) {
			if(headerText != null) header(headerSize, headerText);
			else header(headerSize);
		}
		
		// Make sure that there are questions to display!
		if(questions.size() < 1) return;
		
		// Print top header //
		
		ps("  #");
		for(int i = 0; i < maxIDLength + 2; i++) ps(" ");
		ps((maxTopicLength < 5 ? "T" : "Topic"));
		for(int j = 0; j < maxTopicLength - 2; j++) ps(" ");
		if(maxTopicLength <= 4) {
			for(int i = 0; i < maxTopicLength + (maxTopicLength < 4 ? 2 : 0); i++) ps(" ");
		}
		
		// If, for some reason, the longest question
		// is less than 8 chars, the "Question" string is shortened to "?"
		// with the help of the ternary operator.
		ps(maxNameLength < 8 ? "?" : "Question");
		
		// Code to add the necessary number of spaces in the top header.
		for(int i = 0; i < (maxNameLength - (maxNameLength < 8 ? 1 : 8)) + 3; i++) ps(" ");
		
		ps((maxTimeLength < 15 ? "Next" : "Next review due"));
		
		// The indexes where a special box drawing character (┬ or ┴) should be printed
		int firstIndex = maxIDLength + 3,
			secondIndex = maxIDLength + maxTopicLength + 6,
			thirdIndex = maxIDLength + maxTopicLength + maxNameLength + 9;
		
		// Draw the top border of the question list
		p();
		if(Settings.useBoxDrawing) {
			
			// If box drawing is enabled, then...
			
			ps("┌"); // Draw top left
			for(int i = 1; i < topBottomLen-1; i++) {
				if(i == firstIndex || i == secondIndex || i == thirdIndex) ps("┬");
				else ps(topChar);
			}
			ps("┐"); // Draw top right
		} else {
			for(int i = 0; i < topBottomLen; i++) ps(topChar);
		}
		p();
		
		// Loop over questions a second time, this time actually printing them
		for(int i = 0; i < entries.size(); i++) {
			
			QuestionEntry entry = entries.get(i);
			
			// Print the ID
			ps(String.format("%s %-" + maxIDLength + "s %s ",
							  sideChar, i, sideChar));
			
			// Print 
			TopicColor.printColor(questions.get(startIndex + i).topic.color);
			ps(String.format("%-" + maxTopicLength + "s ", entry.topicStr));
			TopicColor.reset();
			
			ps(sideChar);
			
			TopicColor.printColor((questions.get(startIndex + i).type == QuestionType.FACT ? TopicColor.CYAN : TopicColor.MAGENTA));
			ps(String.format(" %-" + maxNameLength + "s ", entry.nameStr));
			TopicColor.reset();
			
			ps(sideChar);
			
			if(!entry.isToday) { // If the question isn't scheduled for today...
				if(entry.inPast) TopicColor.printColor(TopicColor.RED); // If it's in the past, then make the date red
				else TopicColor.printColor(TopicColor.GREEN); // If it's in the future, then make the date green
			} else {
				TopicColor.printColor(TopicColor.CYAN); // If the question is scheduled for today, make the date cyan
			}
			
			ps(String.format(" %-" + maxTimeLength + "s ", entry.timeStr)); // Print the date
			
			TopicColor.reset(); // Reset color
			
			p(sideChar); // Print closing sideChar
		}
		
		// Draw the bottom border of the question list
		if(Settings.useBoxDrawing) {
			
			// If box drawing is enabled, then...
			
			ps("└"); // Draw bottom left
			for(int i = 1; i < topBottomLen-1; i++) {
				if(i == firstIndex || i == secondIndex || i == thirdIndex) ps("┴"); // If drawing at the border of a column, then draw a special character
				else ps(topChar); // Otherwise, draw a normal top character
			}
			ps("┘"); // Draw bottom right
			
		} else {
			// If box drawing is disabled, then...
			for(int i = 0; i < topBottomLen; i++) ps(topChar); // Draw the bottom lines of the question list with the questionListTopChar
		}
		
		// Print current index in question list
		p("\nViewing " + (startIndex + 1) + " - " + endIndex + " of " + questions.size());
		p();
		
	}
	
	// Returns the days between the given date and today. Used for workloadFormat(LocalDate).
	public static int daysFromToday(LocalDate localDate) {
		return (int) ChronoUnit.DAYS.between(LocalDate.now(), localDate);
	}
	
	// Same as daysFromToday(LocalDate), but takes the absolute value. Used for workloadFormat(LocalDate).
	public static int daysFromTodayAbsolute(LocalDate localDate) {
		return Math.abs(daysFromToday(localDate));
	}
	
	// Returns the number of questions scheduled for the given date.
	// TODO This is horribly inefficient code. It clones the ENTIRE question list
	// for every single workload entry. Fix this inefficiency.
	public static int questionsForDate(LocalDate target) {
		
		// Clone & sort the question list
		ArrayList<Question> clone = new ArrayList<>();
		for(Question q : SecondMemory.list.questions) clone.add(q);
		Collections.sort(clone, sorter);
		
		int questionsForDate = 0;
		for(int i = 0; i < clone.size(); i++) {
			Question question = clone.get(i);
			LocalDate nextReview = question.nextReview;
			if(nextReview.isAfter(target)) break;
			if((!question.reviewedBefore) || nextReview.isBefore(target)) continue; // Continue if question hasn't been reviewed to prevent counting unscheduled questions
			
			questionsForDate++; // If the question's next review date is 
		
		}
		return questionsForDate;
	}
	
	// Prints a Settings.questionListSideChar and a space.
	// Used in the drawWorkload method.
	public static void qs() {
		ps(Settings.questionListSideChar + " ");
	} 
	
	public static void drawWorkload(ArrayList<Question> questions, int daysInFuture) {
		
		// Clone the given ArrayList of questions
		ArrayList<Question> clone = new ArrayList<>();
		for(Question q : questions) clone.add(q);
		
		// Sort it
		Collections.sort(clone, sorter);
		
		// LinkedHashMap to store workload data
		LinkedHashMap<LocalDate, Integer> workload = new LinkedHashMap<>();
		
		
		LocalDate target = LocalDate.now().plusDays(daysInFuture); // The last day to print the workload for.
		
		for(int i = 0; i < questions.size(); i++) {
			Question question = clone.get(i);
			LocalDate nextReview = question.nextReview;
			if(nextReview.isAfter(target)) break;
			if(!question.reviewedBefore) continue; // Continue if question hasn't been reviewed to prevent unscheduled questions showing
			if(!workload.containsKey(nextReview)) {
				workload.put(nextReview, 1);
			} else {
				workload.replace(nextReview, workload.get(nextReview) + 1); // Increment number for question's next review day
			}
		}
		int maxDateLength = 0, maxQuestionNumLength = 0, maxPrettyDateLength = 0; 
		for(Map.Entry<LocalDate, Integer> workloadEntry : workload.entrySet()) {
			LocalDate date = workloadEntry.getKey();
			
			String dateStr = defaultFormat.format(date);
			if(dateStr.length() > maxDateLength) maxDateLength = dateStr.length();
			
			String questionNumStr = String.valueOf(workloadEntry.getValue());
			if(questionNumStr.length() > maxQuestionNumLength) maxQuestionNumLength = questionNumStr.length();
			
			String prettyDateStr = workloadFormat(date);
			if(prettyDateStr.length() > maxPrettyDateLength) maxPrettyDateLength = prettyDateStr.length();
		}
		int topBottomLen = 10 + maxDateLength + maxQuestionNumLength + maxPrettyDateLength;
		ps("  Date");
		for(int i = 0; i < maxDateLength - 1; i++) ps(" ");
		ps("#");
		for(int i = 0; i < maxQuestionNumLength + 2; i++) ps(" ");
		p("Day(s) from today");
		for(int i = 0; i < topBottomLen; i++) ps(Settings.questionListTopChar);
		p();
		for(Map.Entry<LocalDate, Integer> workloadEntry : workload.entrySet()) {
			LocalDate date = workloadEntry.getKey();
			String lineMeatOne = String.format("%-" + maxDateLength + "s", defaultFormat.format(date));
			String lineMeatTwo = String.format("%-" + maxQuestionNumLength + "s", workloadEntry.getValue());
			String lineMeatThree = String.format("%-" + maxPrettyDateLength + "s", workloadFormat(date));
			int questionsNum = questionsForDate(date); // The number of questions for the date
			TopicColor color = TopicColor.GREEN;
			
			// Find the color that the workload entry should be
			if(questionsNum <= Settings.questionLimit) { // Check if number is less than question limit
				// If yes, then perform another check
				if(questionsNum + 2 > Settings.questionLimit) { // Check if question limit will be exceeded if 2 more questions are scheduled for date
					// If yes, then make color red
					color = TopicColor.RED;
				} else {
					// If no, then make color green
					color = TopicColor.GREEN;
				}
			} else {
				// If no (given day is over the question limit), then make color light red
				color = TopicColor.LIGHT_RED;
				
			}
			
			// Find the color that the workload entry should be if it's for today
			TopicColor todayColor = TopicColor.CYAN;
			if(questionsNum <= Settings.questionLimit) { // Check if number is less than question limit
				
				// If number is under question limit, then perform another check
				if(questionsNum + 2 > Settings.questionLimit) { // Check if question limit will be exceeded if 2 more questions are scheduled for date
					// If yes (the day is almost at the limit), then make color cyan
					todayColor = TopicColor.CYAN;
				} else {
					// If no (the day is at least 3 below the limit), then make color light cyan
					todayColor = TopicColor.LIGHT_CYAN;
				}
				
			} else {
				// If today's over the limit, then make color blue
				todayColor = TopicColor.BLUE;
			}
			
			// Print selected color. qs() means insert a Settings.questionListSideChar + a space.
			qs();
			
			TopicColor usedColor = isToday(date) ? todayColor : color; // Determine which color 
			
			// Use ternary to detect if date is today (above). If yes, make this part blue (to indicate today's date).
			TopicColor.printColor(usedColor);
			ps(lineMeatOne + " "); // Print first part (simple date)
			TopicColor.reset(); // Reset color
			
			qs();
			
			TopicColor.printColor(usedColor);
			ps(lineMeatTwo + " "); // Print second part (num. of questions)
			TopicColor.reset();
			
			qs();
			
			TopicColor.printColor(usedColor);
			ps(lineMeatThree + " "); // Print third part (pretty date)
			TopicColor.reset();
			
			p(Settings.questionListSideChar); // Print Settings.questionListSideChar followed by a newline
		}
		for(int i = 0; i < topBottomLen; i++) ps(Settings.questionListTopChar);
		p();
	}
	
	// Used to calculate Levenshtein distance
	public static double similarity(String one, String two) {
		return l.distance(one, two);
	}
	
	// Methods for OS-specific code //
	
	// Used mostly for determining whether ANSI escape codes
	// should be printed or not (they look horrible on Windows, which doesn't support
	// them by default).
	public static void osDependent(Runnable windows, Runnable linux, Runnable mac) {
		if(isWindows) windows.run();
		else if(isLinux) linux.run();
		else if(isMac) mac.run();
	}
	
	
	// The same as the other version of osDependent, except that the same
	// code is run for Mac and Linux. The same code will often work on
	// both platforms, so this method is used much more frequently than the other version.
	public static void osDependent(Runnable windows, Runnable linuxOrMac) {
		osDependent(windows, linuxOrMac, linuxOrMac);
	}
	
	// Header methods //
	
	public static void header(int inBetweenSpace) {
		header(inBetweenSpace, "v" + PROGRAM_VERSION);
	}
	
	// Draws the header, which almost always appears at the top of the screen.
	public static void header(int inBetweenSpace, String text) {
		osDependent(() -> AnsiConsole.system_out.print(Ansi.ansi().bgBright(Color.BLACK).fg(Color.BLACK)), () -> ps("\u001b[47;1m\u001b[30m"));
		ps("🧠² SecondMemory");
		for(int i = 0; i < inBetweenSpace; i++) ps(" ");
		p(text + "\n");
		osDependent(() -> AnsiConsole.system_out.print(Ansi.ansi().reset()), () -> ps("\u001b[0m"));
	}
	
	// Color-printing methods //
	
	public static void basicRedPrintBase(String str) {
		osDependent(() -> { AnsiConsole.system_out.print(Ansi.ansi().fg(Color.RED).a(str).reset()); },
				    () -> { ps("\u001b[31m" + str + "\u001b[0m"); });
	}
	
	public static void basicRedPrint(String str) {
		basicRedPrintBase(str);
		p();
	}
	
	public static void basicGreenPrintBase(String str) {
		osDependent(() -> { AnsiConsole.system_out.print(Ansi.ansi().fg(Color.GREEN).a(str).reset()); },
				    () -> { ps("\u001b[32m" + str + "\u001b[0m"); });
	}
	
	public static void basicGreenPrint(String str) {
		basicGreenPrintBase(str);
		p();
	}
	
	public static void basicYellowPrintBase(String str) {
		osDependent(() -> { AnsiConsole.system_out.print(Ansi.ansi().fg(Color.YELLOW).a(str).reset()); },
				    () -> { ps("\u001b[33m" + str + "\u001b[0m"); });
	}
	
	public static void basicYellowPrint(String str) {
		basicYellowPrintBase(str);
		p();
	}
	
	// File-related methods //
	
	// Checks whether a file exists
	public static boolean exists(String file) {
		return new File(file).exists();
	} 
	
	// Checks whether a file exists in the current
	// working directory
	public static boolean existsInCWD(String file) {
		return exists(CWDS + file);
	}
	
	// Creates a file.
	public static void createFile(String file) {
		try {
			new File(file).createNewFile();
		} catch (IOException e) {
			error(e);
		}
	}

	// Creates a file in the current working directory.
	public static void createFileInCWD(String file) {
		createFile(CWDS + file);
	}
	
	// Creates a folder.
	public static void createFolder(String folder) {
		new File(folder).mkdir();
	}
	
	// Creates a folder in the current working directory.
	public static void createFolderInCWD(String folder) {
		createFolder(CWDS + folder);
	}
	
	// Writes the given text to a file.
	public static void populateFile(String file, String text) throws IOException {
		Files.write(Paths.get(file), text.getBytes());
	}
	
	// Writes the given text to a file in the current working directory.
	public static void populateFileInCWD(String file, String text) throws IOException {
		populateFile(CWDS + file, text);
	}
	
	// Gets the contents of a file.
	public static String readFile(String file) {
		try {
			byte[] bytes = Files.readAllBytes(Paths.get(file));
			return new String(bytes, StandardCharsets.UTF_8);
		} catch (Exception e) {
			error(e);
			return "Couldn't read file";
		}
	}
	
	// Gets the contents of a file in the current
	// working directory.
	public static String readFileInCWD(String file) {
		return readFile(CWDS + file);
	}
	
	// Opens a file in the current working directory
	// using the preferred editor.
	public static void viewFileInCWD(String file) {
		scratchpad.setEditorText(readFileInCWD(file)); // Set scratchpad text to contents of file
		scratchpad.setOpenFile(file); // Set the scratchpad's open file to the file
		scratchpad.showWindow(); // Show the scratchpad
	}
	
	// Various utility methods //
	
	// Used to make errors nicer. Used in almost every
	// try statement in the program.
	public static void error(Exception e) {
		basicRedPrint("An error occurred. Details:");
		e.printStackTrace();
		try {
			Thread.sleep(50); // color will stop before end of stack trace unless this is added
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		osDependent(() -> {}, () -> ps("\u001b[0m"));
	}
	
	// Clears the screen. Works on all platforms.
	public static void clearScreen() {
		osDependent(
		() -> {
			try {
				new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
			} catch (Exception e) {
				error(e);
			}
		}, () -> {
			try {
				new ProcessBuilder("clear").inheritIO().start().waitFor();
			} catch (Exception e) {
				error(e);
			}
		});
	}
	
	// Confirms that the user wants to quit something.
	// Used for question creation and editing.
	public static boolean confirmQuit(String current) {
		ps("Are you sure you want to quit " + current + "? Your work will be lost.\n"
		 + "Type the letter y to continue, or anything else to continue.\n"
		 + "~> ");
		String input = mainScanner.nextLine();
		if(input.equalsIgnoreCase("y")) {
			return true; // Only quit if the user 
		} else {
			return false;
		}
	}
	
	// Testing methods //
	
	// Generates dummy questions with random names and topics
	public static ArrayList<Question> generateSampleData(int length, int arraySize) {
		ArrayList<Question> finalArray = new ArrayList<>();
		for(int j = 0; j < arraySize; j++) {
			Question finished = new Question();
			finished.question = generate(length);
			finished.answer = generate(length);
			finished.topic = new Topic();
			TopicColor randColor = TopicColor.RED;
			int rand = new Random().nextInt(12) + 1;
			switch(rand) {
			case 1:
				randColor = TopicColor.BLUE;
				break;
			case 2:
				randColor = TopicColor.GREEN;
				break;
			case 3:
				randColor = TopicColor.YELLOW;
				break;
			case 4:
				randColor = TopicColor.CYAN;
				break;
			case 5:
				randColor = TopicColor.MAGENTA;
				break;
			case 6:
				randColor = TopicColor.LIGHT_BLUE;
				break;
			case 7:
				randColor = TopicColor.LIGHT_GREEN;
				break;
			case 8:
				randColor = TopicColor.LIGHT_YELLOW;
				break;
			case 9:
				randColor = TopicColor.LIGHT_CYAN;
				break;
			case 10:
				randColor = TopicColor.LIGHT_MAGENTA;
				break;
			case 11:
				randColor = TopicColor.LIGHT_RED;
				break;
			}
			finished.topic = new Topic(generate(length), randColor);
			finalArray.add(finished);
		}
		return finalArray;
	}
	
	// This method generates random text.
	// It was never fully implemented because
	// I never saw the need to.
	private static String generate(int length) {
		String finalStr = "";
		for(int i = 0; i < length; i++) {
			int random = new Random().nextInt(27) + 1;
			String letter = "";
			switch(random) {
			case 1:
				letter = "a";
				break;
			case 2:
				letter = "b";
				break;
			case 3:
				letter = "c";
				break;
			case 4:
				letter = "d";
				break;
			case 5:
				letter = "e";
				break;
			case 6:
				letter = "f";
				break;
			case 7:
				letter = "g";
				break;
			case 8:
				letter = "h";
				break;
			case 9:
				letter = "i";
				break;
			case 10:
				letter = "j";
				break;
			case 11:
				letter = "k";
				break;
			case 12:
				letter = "l";
				break;
			default:
				letter = "";
			}
			finalStr += letter;
		}
		return finalStr;
	}

}
