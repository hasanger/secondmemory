package io.sanger.henry.secondmemory;

import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.AnsiConsole;
import org.fusesource.jansi.Ansi.Color;

public enum TopicColor {
	RED, GREEN, BLUE, YELLOW, MAGENTA, CYAN,
	LIGHT_RED, LIGHT_GREEN, LIGHT_BLUE, LIGHT_YELLOW, LIGHT_MAGENTA, LIGHT_CYAN, DEFAULT;
	
	private static final Ansi ANSI = Ansi.ansi();
	
	public static TopicColor parse(String str) {
		switch(str.toLowerCase()) {
		case "red":
			return TopicColor.RED;
		case "lightred":
		case "light red":
		case "light_red":
			return TopicColor.LIGHT_RED;
		
		case "green":
			return TopicColor.GREEN;
		case "lightgreen":
		case "light green":
		case "light_green":
			return TopicColor.LIGHT_GREEN;
			
		case "blue":
			return TopicColor.BLUE;
		case "lightblue":
		case "light blue":
		case "light_blue":
			return TopicColor.LIGHT_BLUE;
			
		case "yellow":
			return TopicColor.YELLOW;
		case "lightyellow":
		case "light yellow":
		case "light_yellow":
			return TopicColor.LIGHT_YELLOW;
		
		case "magenta":
		case "purple":
			return TopicColor.MAGENTA;
		case "lightmagenta":
		case "light magenta":
		case "light_magenta":
		case "pink":
			return TopicColor.LIGHT_MAGENTA;
			
		case "cyan":
			return TopicColor.CYAN;
		case "lightcyan":
		case "light cyan":
		case "light_cyan":
			return TopicColor.LIGHT_CYAN;
			
		default:
			return TopicColor.DEFAULT;
		}
	}
	
	public static void printColor(TopicColor color) {
		Utils.osDependent(() -> {
			Color f = Color.DEFAULT;
			boolean bright = false;
			switch(color) {
			case DEFAULT:
				return;
			default:
				return;
			case RED:
				f = Color.RED;
				break;
			case GREEN:
				f = Color.GREEN;
				break;
			case YELLOW:
				f = Color.YELLOW;
				break;
			case BLUE:
				f = Color.BLUE;
				break;
			case MAGENTA:
				f = Color.MAGENTA;
				break;
			case CYAN:
				f = Color.CYAN;
				break;
				
			case LIGHT_RED:
				f = Color.RED;
				bright = true;
				break;
			case LIGHT_GREEN:
				f = Color.GREEN;
				bright = true;
				break;
			case LIGHT_YELLOW:
				f = Color.YELLOW;
				bright = true;
				break;
			case LIGHT_BLUE:
				f = Color.BLUE;
				bright = true;
				break;
			case LIGHT_MAGENTA:
				f = Color.MAGENTA;
				bright = true;
				break;
			case LIGHT_CYAN:
				f = Color.CYAN;
				bright = true;
				break;
			}
			if(!bright) AnsiConsole.system_out.print(ANSI.fg(f));
			else AnsiConsole.system_out.print(ANSI.fgBright(f));
		}, () -> {
			switch(color) {
			case DEFAULT:
				return;
			default:
				return;
			case RED:
				ep("31m");
				break;
			case GREEN:
				ep("32m");
				break;
			case YELLOW:
				ep("33m");
				break;
			case BLUE:
				ep("34m");
				break;
			case MAGENTA:
				ep("35m");
				break;
			case CYAN:
				ep("36m");
				break;
				
			case LIGHT_RED:
				ep("31;1m");
				break;
			case LIGHT_GREEN:
				ep("32;1m");
				break;
			case LIGHT_YELLOW:
				ep("33;1m");
				break;
			case LIGHT_BLUE:
				ep("34;1m");
				break;
			case LIGHT_MAGENTA:
				ep("35;1m");
				break;
			case LIGHT_CYAN:
				ep("36;1m");
				break;
			}
		});
	}
	
	private static void ep(String color) {
		System.out.print("\u001b[" + color);
	}
	
	public static void reset() {
		Utils.osDependent(() -> AnsiConsole.system_out.print(ANSI.reset()), () -> System.out.print("\u001b[0m"));
	}
	
}
