package io.sanger.henry.secondmemory;

/**
 * The {@code Topic} class
 * is like a tag, except that
 * there is exactly one for each
 * question.
 * @author gitlabcyclist
 * @version 0.4 beta
*/
public class Topic {
	
	public String name = ""; // The topic's name
	public TopicColor color = TopicColor.DEFAULT; // The topic's color
	public boolean staySame = false; // Determines whether or not the topic should stay the same when editing
	
	public Topic() {}
	
	public Topic(String name, TopicColor color) {
		this.name = name;
		this.color = color;
	}
	
	public Topic(boolean staySame) {
		this.name = null;
		this.color = null;
		this.staySame = staySame;
	}

}
