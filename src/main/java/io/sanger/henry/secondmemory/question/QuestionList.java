package io.sanger.henry.secondmemory.question;

import java.util.ArrayList;

import org.json.JSONStringer;

import io.sanger.henry.secondmemory.SecondMemory;
import io.sanger.henry.secondmemory.Utils;

/**
 * The {@code QuestionList} class
 * is a wrapper for an {@code ArrayList}
 * of questions, with some additional helper
 * methods.
 * @author gitlabcyclist
 * @version 0.4 beta
*/
public class QuestionList {
	
	public ArrayList<Question> questions = new ArrayList<>(); // The internal list of questions
	
	public int screenStart = 0, screenEnd = 9; // The start and end of the screen
	
	// Converts the question list to JSON, for storage
	public String toJSON() {
		JSONStringer listStringer = new JSONStringer();
		listStringer.object()
					.key("questions").array();
		// A more complex for loop is used
		// to retain order of questions
		for(int i = 0; i < questions.size(); i++) {
			Question question = questions.get(i);
			listStringer.object()
						.key("question").value(question.question)
						.key("answer").value(question.answer)
						.key("topicName").value(question.topic.name)
						.key("type").value(QuestionType.toString(question.type))
						.key("latestScore").value(question.latestScore)
						.key("latestLevenshtein").value(question.latestLevenshtein)
						.key("lastReview").value(Utils.jsonFormat(question.lastReview))
						.key("nextReview").value(Utils.jsonFormat(question.nextReview))
						.key("reviewedBefore").value(question.reviewedBefore)
						.key("previousScores").array();
			for(int prevScore : question.scores) listStringer.value(prevScore);
			listStringer.endArray()
						.key("previousLevenshteins").array();
			for(double prevLevenshtein : question.allLevenshteins) listStringer.value(prevLevenshtein);
			listStringer.endArray()
						.endObject();
		}
		listStringer.endArray()
					.endObject();
		return listStringer.toString();
	}
	
	// This method calls the Utils.draw method
	// on the internal ArrayList of questions.
	public void draw(boolean header) {
		Utils.draw(header, null, questions, screenStart, screenEnd);
	}
	
	// This method calls the Utils.draw method
	// on the internal ArrayList of questions.
	public void draw(boolean header, String headerText) {
		Utils.draw(header, headerText, questions, screenStart, screenEnd);
	}
	
	// Goes to the next screen of questions.
	public void nextScreen() {
		if(screenEnd + 10 < (questions.size() + 9)) {
			screenStart += 10;
			screenEnd += 10;
			if(screenEnd - screenStart != 9) { // each screen should contain exactly 10 questions
				SecondMemory.floatText = "Something went wrong, so the pagination was reset.";
				SecondMemory.floatTextErroneous = true;
				screenStart = 0;
				screenEnd = 9;
			}
		}
	} 
	
	// Goes to the previous screen of questions.
	// This method calculates the nearest multiples of 10,
	// so that when navigating back from the last page (if it has
	// less than 10 questions), the pagination isn't messed up.
	public void previousScreen() {
		int oldScreenEnd = screenEnd;
		screenEnd -= 10;
		int i = screenEnd;
		for(; !String.valueOf(i).endsWith("0"); i--); // calculate nearest multiple of 10
		screenStart = i;
		// Reset if screen start is less than 0
		if(screenStart < 0) {
			screenEnd = oldScreenEnd;
			screenStart = 0;
		}
		int j = screenStart + 1; // +1 so screenStart (which ends in 0) isn't counted
		for(; !String.valueOf(j).endsWith("0"); j++); // calculate nearest multiple of 10
		screenEnd = j - 1; // -1 because there's one extra
	}
	
	// Goes to the first screen of questions
	// (resets the pagination).
	public void firstScreen() {
		screenStart = 0;
		screenEnd = 9;
	} 
	
	// Goes to the last screen of questions
	public void lastScreen() {
		screenEnd = questions.size() + 1;
		int i = questions.size();
		for(; i % 10 != 0; i--); // calculate nearest multiple of 10
		Utils.p("Hello! " + Integer.valueOf(screenEnd - i).toString());
		if(screenEnd - i < 2) i -= 10; // Handle a special case
		Utils.p("i = " + i);
		screenStart = i;
	}

}
