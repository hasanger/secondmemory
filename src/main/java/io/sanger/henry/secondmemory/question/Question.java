package io.sanger.henry.secondmemory.question;

import java.time.LocalDate;
import java.util.ArrayList;

import io.sanger.henry.secondmemory.Topic;
import io.sanger.henry.secondmemory.TopicColor;
import io.sanger.henry.secondmemory.Utils;

/**
 * The {@code Question} class is the base class
 * of all questions.
 * @author gitlabcyclist
 * @version 0.4 beta
*/
public class Question {
	
	public String question, answer; // The question and answer
	
	public Topic topic = new Topic("Default topic", TopicColor.CYAN);
	
	public double latestLevenshtein = 0.0;
	
	public double averageLevenshtein = 0.0;
	
	public ArrayList<Double> allLevenshteins = new ArrayList<>();
	
	public int latestScore = 0;
	
	public double averageScore = 0.0;
	
	public ArrayList<Integer> scores = new ArrayList<>();
	
	public LocalDate lastReview = LocalDate.now(), nextReview = LocalDate.now();
	
	public boolean reviewedBefore = false;
	
	public QuestionType type = QuestionType.FACT;
	
	public ArrayList<String> previousAnswers = new ArrayList<>();
	
	public Question() {}
	
	public Question(Topic topic) {
		this.topic = topic;
	}
	
	public Question(String question, String answer, Topic topic) {
		this.question = question;
		this.answer = answer;
		this.topic = topic;
	}
	
	public void score(int score, String entry) {
		this.latestScore = score;
		scores.add(score);
		this.latestLevenshtein = Utils.similarity(entry, answer);
		allLevenshteins.add(latestLevenshtein);
	}
	
	public double calculateAverageScore() {
		if(scores.size() < 1) return 0.0;
		int added = 0;
		for(int score : scores) added += score;
		double finished = (double) (added / scores.size());
		averageScore = finished;
		return finished;
	} 
	
	public double calculateAverageLevenshtein() {
		if(allLevenshteins.size() < 1) return 0.0;
		int added = 0;
		for(double levenshtein : allLevenshteins) added += levenshtein;
		double finished = (double) (added / allLevenshteins.size());
		averageLevenshtein = finished;
		return finished;
	}
	
	// Changes the next review and last review date,
	// as is required after reviewing a question.
	public void changeDates(LocalDate nextReview) {
		reviewedBefore = true;
		this.nextReview = nextReview; // Set next review
		this.lastReview = LocalDate.now(); // Set last review to now
	}
	
	// The difference between this method and the above one
	// is that the reschedule method sets the last review to today,
	// which happens when the question is reviewed. However, when
	// rescheduling, that isn't what we want (since this would indicate
	// that the question has been reviewed when it hasn't). This method
	// simply sets the next review date of this Question.
	public void reschedule(LocalDate nextReview) {
		this.nextReview = nextReview; // Set next review
	}
	
	public Question clone() {
		Question cloned = new Question();
		
		cloned.question = this.question;
		cloned.answer = this.answer;
		cloned.type = this.type;
		cloned.topic.name = this.topic.name;
		cloned.topic.color = this.topic.color;
		cloned.reviewedBefore = this.reviewedBefore;
		
		cloned.lastReview = this.lastReview;
		cloned.nextReview = this.nextReview;
		
		cloned.previousAnswers = this.previousAnswers;
		
		cloned.scores = this.scores;
		cloned.latestScore = this.latestScore;
		cloned.averageScore = this.averageScore;
		
		cloned.allLevenshteins = this.allLevenshteins;
		cloned.latestLevenshtein = this.latestLevenshtein;
		cloned.averageLevenshtein = this.averageLevenshtein;
		
		
		return cloned;
	}

}
