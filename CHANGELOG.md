# Later features
- Tutorials on setting up the server website (maybe a Heroku deploy button?)

# 1.0 
## Planned features
- Web app and REST API
- Ability to connect to a server
- Default server configuration option in settings.json
- Charts with insights
- History (list of recently reviewed questions)
- Bibliography
- Ability to "connect" various related questions
- View web of connected questions
- Anki & Mnemosyne import
- Integration with TaskJourney (another project, coming soon!)

# 0.4 beta
## Planned features (crossed out = done)
### Usability
- Ability to create, edit, and delete topics & change settings from within the program
- Basic spaced repetition
- Backup system
- Option to record answers to questions
- Ability to change next review date of questions without reviewing
- Suggested score
- Achievements
- Stats screen with workload & other tidbits
- List questions
- ~~Allow exiting at "Enter your answer" stage of question review~~
- Option to go back when creating, editing, and reviewing questions
- Option to skip to the end when editing questions
- Quicker review (offer to move on to the next question)
- Count questions done today
- Hints, shown when you're stuck
- Filter by topic
- Option to view number of questions for a certain day from question review
- Ability to edit and delete questions from within the question view
- Ability to change workload size
- ~~Customizable question limit~~
- ~~Show a warning when you try to schedule a question for a date which has reached the limit~~
- Option to disable header
- ~~Built-in Swing-based editor~~
- ~~ Move questions (ability to "push" questions back or forward by multiple days)~~
- Ability to reschedule individual questions without reviewing them
### Aesthetic
- ~~Remove SLF4J message~~
- ~~Box around stats screen~~
- Make commands screen, stats screen, and workload boxes use question list side and top chars
- ~~Even better date parsing~~
- Add support for box drawing characters
- Add support for changing "~>" input prompt
- Add "~>" input prompt in missing places (in progress)
- Make the header hidable with a configuration option
- Make questions with a "not scheduled" date always have a yellow color
### Code
- Clean up, organize, add more comments
- Make a sort() method to sort questions, that also moves "not scheduled" questions to the very beginning

# 0.3c alpha
## New features & fixes
- Fix bug with pagination in search
- Basic Q&A import (SuperMemo, possibly others)
- Installer for Windows & Linux
- Ability to edit question type
- Ability to turn off auto move on when editor closes
- Ability to override configuration directory from settings.json
- Better date parsing (today and tomorrow instead of x hours from now)
- --use-home-dir command-line argument. If passed, SecondMemory uses the ".secondmemory" folder in the user's home directory for questions, topics, and settings.

# 0.3b alpha 
## New features
- Important bug fixes

# 0.3 alpha 
## New features
- Editing and deleting of questions
- Better error handling
- Ability to exit question creation at any stage
- Support for colors on Windows
- Different colors for narrative and fact questions to tell them apart

# 0.2 alpha
## New features
- Saving and loading of questions
- Due dates for questions
- Auto-sort by date
- Proper review system
- Pagination
- Calculation of Levenshtein distance between given answer and correct answer
- Stats screen
- Customizable prompt

# 0.1 alpha
## New features
- Fact questions
- Question list
- Help
