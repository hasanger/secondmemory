![SecondMemory](https://gitlab.com/hasanger/secondmemory/raw/master/secondmemory-logo-2.png "SecondMemory")
WARNING: The contents of this repository is bleeding-edge, often untested code. If you're using SecondMemory for important data, I recommend that you use a [stable release](https://gitlab.com/hasanger/secondmemory/-/releases).

<!---
https://img.shields.io/badge/stable%20version-0.3c-green.svg
https://img.shields.io/badge/next%20version-0.4-orange.svg
-->

<a href="https://gitlab.com/hasanger/secondmemory/tree/stable">![Stable version](https://gitlab.com/hasanger/secondmemory/raw/master/stable.svg?inline=false)</a>
<a href="https://gitlab.com/hasanger/secondmemory">![Next version](https://gitlab.com/hasanger/secondmemory/raw/master/next.svg?inline=false)</a>

## What is it?
A command-line Java program to help you remember things. It's portable, customizable, and easy to use once you get the hang of it.

Grab the latest stable version from [the releases page](https://gitlab.com/hasanger/secondmemory/-/releases). You can also check out [the website](https://hasanger.gitlab.io/secondmemory-website/index.html).

## Gallery
![SecondMemory preview](https://gitlab.com/hasanger/secondmemory/raw/master/secondmemory.png)

![SecondMemory retro preview](https://gitlab.com/hasanger/secondmemory/raw/master/secondmemory-retro.png)

The second image was made using cool-retro-term:
https://github.com/Swordfish90/cool-retro-term

## What makes it different from other spaced repetition programs?
It requires you to type out the answers to questions. Programs like SuperMemo have you think of the answer and then grade yourself. In my experience, you can just "sort of" think of the answer, and then give it a grade. SecondMemory makes you type it out, forcing you to think hard about it, instead of getting a vague impression of the question and the answer. (You also get a good idea of the question being asked, because typing out answers takes a bit longer, giving you time to think about it.) All these factors work together to help you remember things better than a program like SuperMemo or Anki.

Note: if you're looking for a good program to help you learn programming, consider Revuu (https://www.github.com/globewalldesk/revuu).

## Can I import my data from other programs?
Yes, if you're using a program that can export your questions in a Q&A format (e.g. SuperMemo):
```
Q: Here's a question
A: Here's an answer

Q: Another question
A: And an answer
```
Be warned that data _about_ the questions (like interval, next review, last review, etc.) will not be saved. Support for more programs (and export) is coming soon.

## That's cool! How do I get started?
Visit the [help page](https://gitlab.com/hasanger/secondmemory/wikis/Help) for installation instructions.

## Changelog
See [CHANGELOG.md](https://gitlab.com/hasanger/secondmemory/blob/master/CHANGELOG.md).
